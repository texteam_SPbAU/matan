\setauthor{ Белоусов Н.}

\begin{theorem}
(Теорема Вейерштрасса.) \\
Рассмотрим $f \in C[a,b]$ и $\eps > 0$. Тогда существует $P$ -- многочлен такой, что
	\[ \forall x \in [a,b] \colon |f(x) - P(x)| < \eps. \]
Иначе говоря,
	\[ \| f - P \|_{C[a,b]} < \eps. \]
\end{theorem}

\begin{proof}
Определим функцию $g$ на $[0, \pi]$:
	\[ g(t) = f(a+ \tfrac{b-a}{\pi} t). \]
На $[-\pi,0]$ продолжим по чётности. Тогда, очевидно, $g$ непрерывная, и $g(\pi) = g(-\pi)$. По предыдущей теореме: существует $T$ -- тригонометрический многочлен такой, что 
	\[ \forall t \in [-\pi, \pi] \colon |g(t) - T(t)| < \eps. \]
Как обычно:
\begin{gather*}
	T(t) = \frac{a_0}{2} + \sum_{k=1}^n \bigl( a_k \cos kt + b_k \sin kt \bigl); \\
	\cos kt = \sum_{j=0}^\infty \frac{(-1)^j (kt)^{2j}}{(2j)!}, \qquad \sin kt = \sum_{j=0}^\infty \frac{(-1)^j (kt)^{2j+1} }{(2j+1)!}. 
\end{gather*}
Ряды для косинуса и синуса сходятся везде, значит, на любом конечном отрезке сходятся равномерно (теорема из прошлого семестра). Стало быть, хвосты рядов $ \rra 0$. Будем так обрезать эти ряды, чтобы даже с учётом умножения на $a_k$ и $b_k$ хвосты все вместе были меньше $\eps$. Например, обрежем так, что 
	\[ \text{любой хвост} < \frac{\eps}{\sum_{k=1}^\infty \bigl( |a_k| + |b_k| \bigl)} . \]
То есть существует такой многочлен $Q$, что
	\[ \forall t \in [0,\pi] \colon |T(t) - Q(t)| < \eps \quad \Ra \quad |Q(t) - g(t)| < 2\eps. \]
Т.к. $ f(x) = g \bigl( (x-a) \frac{\pi}{(b-a)} \bigl)$, то искомый многочлен
	\[ P(x) = Q \bigl( (x-a) \tfrac{\pi}{(b-a)} \bigl). \]
\end{proof}

\begin{consequence}
Для всякой функции $f \in C[a,b]$ существует $P_n$ -- последовательность многочленов такая, что $P_n \rra f$ на $[a,b]$.
\end{consequence}

\begin{definition}
$X$ -- метрическое пространство функций на $[a,b]$, $Y \subset X$. \\
Система функций $\phi_1, \phi_2, ... \colon [a,b] \ra \R$ (или $\C$) полна в $Y$ (по метрике пространства $X$), если 
	\[ \forall f \in Y, \forall \eps > 0 \, \exists n, \exists \lambda_1, \lambda_2,..., \lambda_n \in \R (\C) \colon \rho( f, \lambda_1 \phi_1 + \lambda_2 \phi_2 + \ldots + \lambda_n \phi_n) < \eps. \]
\end{definition}

\begin{definition}
$\phi_1, \phi_2, ... \colon [a,b] \ra \R$ -- система функций. Тогда $\Lin \{ \phi_n \}_{n=1}^\infty$ -- множество всех конечных линейных комбинаций $\phi_n$.
\end{definition}

\begin{warning}
Система полна в $Y$ тогда и только тогда, когда $\cl \Lin \{ \phi_n \}_{n=1}^\infty \supset Y$ (замыкание в смысле метрики из $X$).
\end{warning}

Посмотрим на первую теорему Вейерштрасса в терминах этих определений.
\begin{theorem}
Система функций $1, \cos x, \sin x, \cos 2x, \sin 2x, ...$ полна в множестве непрерывных на $[-\pi,\pi]$ функций, которые принимают равные значения на концах (по метрике пространства $C[-\pi,\pi]$). 
\end{theorem}

И на вторую.
\begin{theorem}
Система функций $1,x,x^2, ...$ полна в $C[a,b]$.
\end{theorem}

\begin{warning}
Была лемма (2.1.2.1) про приближение абсолютно интегрируемой функции ступенчатыми. В новых терминах: система характеристических функций полна в множестве абсолютно интегрируемых функций (в смысле метрики $\rho(f,g) = \int |f-g|$).
\end{warning}

\section{Неравенство Бесселя}

\begin{definition}
Функция $f \colon [a,b] \ra \R$ (или $\C$) суммируема с квадратом, если $|f|^2$ -- абсолютно интегрируема. 
\end{definition}

\begin{warning}
Суммируемость с квадратом влечёт абсолютную интегрируемость.
\end{warning}

\begin{proof}
	\[ |f| \leq \frac{1+|f|^2}{2}. \]
\end{proof} 

\begin{warning}
Но не наоборот.
\end{warning}

\begin{proof}
$f = \frac{1}{\sqrt{|x|}}$ абсолютно интегрируема на $[-1,1]$, но не суммируема с квадратом.
\end{proof}

\paragraph{Скалярное произведение.} \- \par
$f,g \colon [a,b] \ra \R (\C)$ суммируемы с квадратом. Скалярное произведение $f$ и $g$:
	\[ \langle f,g \rangle = \int\limits_a^b f(x) \overline{g(x)} \, dx. \]
Почему определено корректно:
	\[ |f(x) \overline{g(x)}| \leq |f(x)| |g(x)| \leq \frac{|f(x)|^2 + |g(x)|^2}{2}. \]
Есть норма:
	\[ \| f \|^2 = \langle f, f \rangle = \int\limits_a^b |f(x)|^2 \, dx. \]
Можно говорить про сходимость по этой норме или ещё чего. \\
Теперь будем говорить только про функции на $[-\pi, \pi]$:
\begin{gather*}
	1, \cos x, \sin x, ... \text{ -- ортогональная система функций}; \\
	\frac{1}{\sqrt{2\pi}} , \frac{\cos x}{\sqrt{\pi}}, \frac{\sin x}{\sqrt{\pi}}, ... \text{ -- ортонормированная система функций}; \\
	\{ e^{inx} \}_{n=-\infty}^{n=+\infty} \text{ -- ортогональная система функций}; \\
	\left\{ \frac{e^{inx}}{\sqrt{2\pi}} \right\}_{n=-\infty}^{n=+\infty} \text{ -- ортономированная система функций}.
\end{gather*}

Коэффициенты Фурье в терминах скалярного произведения:
\begin{gather*}
	a_k = \frac{1}{\pi} \int\limits_{-\pi}^\pi f(x) \cos kx \, dx = \frac{\langle f, \cos kx \rangle}{\| \cos kx \|^2}; \\
	b_k = \frac{1}{\pi} \int\limits_{-\pi}^\pi f(x) \sin kx \, dx = \frac{\langle f, \sin kx \rangle}{\| \sin kx \|^2}.
\end{gather*}

\begin{definition}
$e_0,e_1,...$ -- ортогональная система, $f$ -- некоторая функция. \\
Коэффициенты Фурье функции $f$:
	\[ c_k(f) = \frac{\langle f, e_k \rangle}{\| e_k \|^2}. \]
Ряд Фурье функции $f$ по ортогональной системе $\{ e_k \}$: 
	\[ \sum_{k=0}^\infty c_k(f) e_k. \]
\begin{center}
(Это всё только формальные определения.)
\end{center}
Заметим, что, домножая $e_k$ на константу, ряд мы не поменяем.
\end{definition}

\begin{theorem}
$\{ e_n \}$ -- ортогональная система, $L = \Lin \{ e_1, e_2, ... \}$. \\
$f$ суммируема с квадратом, $S_n$ -- частные суммы ряда Фурье для функции $f$:
	\[ S_n = \sum_{k=1}^n c_k(f) e_k. \]
Тогда 
\begin{enumerate}
\item $f = S_n + g$, где $g \, \bot \, L$ (т.е. $\bot$ каждому $e_k$).
\item $\| f - S_n \| = \underset{h \in L}{\min} \| f - h \|$.
\item $\|S_n\| \leq \|f\|$.
\end{enumerate}
\end{theorem}

\begin{proof}
\begin{enumerate}
\item 
	\[ \langle f - S_n , e_k \rangle = \langle f, e_k \rangle - \langle S_n, e_k \rangle = \langle f, e_k \rangle - \bigl\langle \sum_{j=1}^n c_j(f) e_j, e_k \bigl\rangle = \langle f, e_k \rangle - c_k(f) \| e_k \|^2 = 0. \]
\item Рассмотрим $h \in L$:
	\[ f - h = \underbrace{(f - S_n)}_{\bot L} + \underbrace{(S_n - h)}_{\in L} \quad \Ra \quad (f-S_n) \bot (S_n - h) \quad \Ra \quad \| f - h \|^2 = \| f - S_n \|^2 + \|S_n - h \|^2.\]
Последний переход -- теорема Пифагора для произвольного скалярного произведения. Доказать просто: рассмотрите $\| a+ b \|^2 = \langle a+ b, a+b \rangle = ...$ -- дальше по линейности. \\
Итак, 
	\[ \| f-h \| \geq \|f - S_n \|, \quad \forall h \in L \text{ (при $h=S_n$ имеем равенство)} \quad \Ra \quad \|f-S_n\| = \underset{h \in L}{\min} \| f - h \|. \]
\item 
	\[ f = \underbrace{(f-S_n)}_{\bot L} + \underbrace{S_n}_{\in L} \quad \Ra \quad \|f\|^2 = \| f - S_n \|^2 + \| S_n \|^2 \geq \|S_n \|^2. \]
\end{enumerate}
\end{proof}

\begin{consequence}
(Неравенство Бесселя.) \\
	\[ \sum_{k=0}^\infty | c_k(f) |^2 \| e_k \|^2 \leq \| f \|^2. \]
\end{consequence}

\begin{proof}
	\[ \| S_n \|^2 = \langle S_n , S_n \rangle = \bigl\langle \sum_{k=0}^n c_k(f) e_k, \sum_{j=0}^n c_j(f) e_j \bigl\rangle = \sum_{k=0}^n \langle c_k(f) e_k , c_k(f) e_k \rangle = \sum_{k=0}^n c_k(f) \overline{c_k(f)} \langle e_k, e_k \rangle = \sum_{k=0}^n | c_k(f) | \|e_k \|^2. \]
А по предыдущей теореме $\|S_n \|^2 \leq \|f \|^2$. Дальше делаем предельный переход $n \ra \infty$ и получаем неравенство Бесселя. 
\end{proof}

\begin{warning}
Если $\{ e_n \}$ -- ортонормированная система, то
	\[ \sum_{k=0}^\infty | \langle f,e_k \rangle |^2 \leq \| f \|^2. \]
Геометрический смысл: квадрат длины вектора $\geq$ суммы квадратов длин проекций на некоторую систему функций (необязательно полную). 
\end{warning}

\- \\
Вернёмся к обычным (тригонометрическим) рядам Фурье.
\begin{consequence}
(Минимальное свойство ряда Фурье.) \\
$f \colon [-\pi, \pi] \ra \R$ суммируема с квадратом, $S_n$ -- частичные суммы ряда Фурье. \\
Тогда 
	\[ \|f-S_n\|^2 = \underset{T}{\min} \|f-T\|^2, \] 
где $T$ -- тригонометрический многочлен степени $n$.
\end{consequence}

\begin{proof}
Смотри теорему 2.4.1, пункт 2.
\end{proof}

\begin{consequence}
(Неравенство Бесселя.) \\
$f \colon [-\pi , \pi] \ra \R$ суммируема с квадратом. Тогда	
	\[ \frac{a_0^2}{2} + \sum_{n=1}^\infty (a_n^2 + b_n^2) \leq \frac{1}{\pi} \int\limits_{-\pi}^\pi |f(x)|^2 \, dx. \]
\end{consequence}

\begin{proof}
Это просто неравенство Бесселя, применённое к обычным рядам Фурье; надо только проверить, что все коэффициенты правильные:
\begin{gather*}
	\| 1 \|^2 = 2\pi, \quad \| \cos kx \|^2 = \| \sin kx \|^2 = \pi; \\
	a_k = \frac{\langle f, \cos kx \rangle}{\| \cos kx \|^2 } = c_{...}(f); \\
	b_k = \frac{\langle f, \sin kx \rangle}{\| \sin kx \|^2 } = c_{...}(f); \\
	a_0 = 2 \cdot \frac{ \langle f, 1 \rangle \rangle}{\|1 \|^2} = 2c_{...}(f). 
\end{gather*}
\end{proof}

\begin{warning}
На самом деле, в данном случае не неравенство, а равенство (равенство Парсеваля).
\end{warning}

\begin{warning}
Можно доказать, что равенство будет для любой полной системы.
\end{warning}

\paragraph{Комплексная запись рядов Фурье.} 
\begin{gather*}
	1, \cos x, \sin x, ... \qquad \text{коэффициенты: } a_k, b_k; \\
	\{e^{inx}\}_{n=-\infty}^{+\infty} \qquad \text{коэффициенты: } c_k. 
\end{gather*}
Поймём, как эти коэффициенты связаны:
	\[ a_k = \frac{\langle f , \cos kx \rangle}{\pi} = \frac{\langle f, e^{ikx} \rangle + \langle f, e^{-ikx} \rangle}{2\pi} = \frac{\langle f, e^{ikx} \rangle}{\| e^{ikx} \|^2} + \frac{\langle f, e^{-ikx} \rangle}{\|e^{-ikx} \|^2} = c_k + c_{-k}. \]
При этом $a_0 = c_0$. Точно так же:
	\[ b_k = \frac{\langle f, \sin kx \rangle}{\pi} = \frac{\langle f, \frac{i}{2} (e^{-ikx} - e^{ikx}) \rangle}{\pi} = -\frac{i}{2\pi} \bigl( \langle f, e^{-ikx} \rangle - \langle f, e^{ikx} \rangle \bigl) = i(c_k - c_{-k}). \]
И последнее наблюдение:
	\[ c_k(f) = \frac{1}{2\pi} \int\limits_{-\pi}^\pi f(x) e^{-ikx} \, dx. \]
Если $f$ вещественнозначна, то
	\[ \overline{c_k(f)} = \frac{1}{2\pi} \int\limits_{-\pi}^\pi f(x) e^{ikx} \, dx = c_{-k}(f). \]
