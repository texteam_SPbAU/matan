\setauthor{ Белоусов Н.}

\chapter{Ряды Фурье}

\section{Ряды Фурье}

\begin{definition}
Ряд 
	\[ \frac{a_0}{2} + \sum_{n=1}^{+\infty} ( a_n \cos nx + b_n \sin nx ) \]
называется тригонометрическим. \par
Тригонометрический многочлен, соответственно:
	\[ P = \frac{a_0}{2} + \sum_{n=1}^N ( a_n \cos nx + b_n \sin nx ). \]
$ \deg P = N $, если $a_N \not= 0$ или $b_N \not= 0$, но $a_n = b_n = 0$ при $n > N$. 
\end{definition}

\begin{warning}
Частичные суммы тригонометрического ряда есть тригонометрические многочлены.
\end{warning}

\begin{example} 
Поймём, чему равны
	\[ \sum_{n=0}^{+\infty} r^n \cos n \phi \; \text{ и } \; \sum_{n=0}^{+\infty} r^n \sin n \phi \quad (|r| < 1). \]
Очевидно, что ряды сходятся (мажорируем геометрической прогрессией).
\begin{gather*}
	S_N = \sum_{n=0}^N r^n \cos n \phi + i \sum_{n=0}^N r^n \sin n \phi = \sum_{n=0}^N (r e^{i \phi})^n \text{ -- геом. прогрессия} \quad \Ra \\
	\Ra \quad S_N = \frac{(re^{i \phi})^{N+1} - 1}{re^{i \phi} - 1} \underset{N \ra \infty}{\longrightarrow} \frac{1}{1 - r e^{i \phi}} = \frac{1-r e^{-i \phi}}{|1 - r e^{i \phi}|^2} = \frac{(1-r \cos \phi) + i r\sin \phi}{(1 - r \cos \phi)^2 + (r \sin \phi)^2} = \frac{(1-r \cos \phi) + i r\sin \phi}{1 - 2r \cos \phi + r^2}.
\end{gather*}
Значит,
\begin{align*}
	\sum_{n=0}^{+\infty} r^n \cos n \phi & = \frac{1 - r \cos \phi}{1 - 2r \cos \phi + r^2}; \\
	\sum_{n=0}^{+\infty} r^n \sin n \phi & = \frac{ r \sin \phi}{1 - 2r \cos \phi + r^2}. 
\end{align*}
\end{example}

\begin{lemma}
\begin{gather*}
	\int\limits_0^{2\pi} \sin (nx) \sin (mx) \, dx = \int\limits_0^{2\pi} \cos (nx) \cos (mx) \, dx = 0 \quad (m \not= n \in \N_0), \\
	\int\limits_0^{2\pi} \sin (nx) \cos (mx) \, dx = 0 \quad (\forall m, n \in \N_0), \\
	\int\limits_0^{2\pi} \sin^2 (nx) \, dx = \int\limits_0^{2\pi} \cos^2 (nx) \, dx = \pi \quad (n \not= 0).
\end{gather*}
\end{lemma}

\begin{proof}
Докажем какое-нибудь одно равенство:
	\[ (n \not= m) \quad \int\limits_0^{2\pi} \cos (mx) \cos (nx) \, dx =  \int\limits_0^{2\pi} \frac{ \cos \bigl( (n-m) x \bigl) + \cos \bigl( (n+m) x \bigl) }{2} \, dx = \frac{ \sin \bigl( (n-m) x \bigl) }{2 (n-m)} \biggl|_0^{2\pi}  + \frac{ \sin \bigl( (n+m) x \bigl) }{2 (n+m)} \biggl|_0^{2\pi} = 0. \]
Остальные докажите сами.
\end{proof}

\begin{theorem}
	\[ f(x) = \frac{a_0}{2} + \sum_{n=1}^{+\infty} ( a_n \cos nx + b_n \sin nx ) \]
равномерно сходится на $[ - \pi, \pi ]$. Тогда
	\[ a_n = \frac{1}{\pi} \int\limits_{-\pi}^\pi f(x) \cos (nx) \, dx, \qquad b_n = \frac{1}{\pi} \int\limits_{-\pi}^\pi f(x) \sin (nx) \, dx. \]
\end{theorem}

\begin{proof}
	\[  \int\limits_{-\pi}^\pi f(x) \cos (nx) \, dx = \int\limits_{-\pi}^\pi \left( \sum_{k=1}^{+\infty} ( a_k \cos kx + b_k \sin kx ) + \frac{a_0}{2} \right) \cos (nx) \, dx = ... \]
Ряд под интегралом по-прежнему равномерно сходится, т.к. домножили равномерно сходящийся ряд (почленно) на ограниченную функцию. Значит, можем менять интеграл и сумму местами:
	\[ ... = \sum_{k=1}^{+\infty} \Biggl( \, \underbrace{\int\limits_{-\pi}^\pi a_k \cos (kx) \cos (nx) \, dx}_{=\delta_{kn} \pi a_k} + \underbrace{\int\limits_{-\pi}^\pi b_k \sin (kx) \cos (nx) \, dx}_{=0} \Biggl) + \underbrace{\int\limits_{-\pi}^\pi \frac{a_0 \cos (nx)}{2} \, dx}_{=\delta_{0n} \pi a_0} = \pi a_n. \]
Для $b_n$ аналогично.
\end{proof}

\paragraph{Отступление про сходимость интегралов.} \- \par
Рассмотрим $f \colon [a,b] \ra \R$. \par
Во-первых, допустим, что существует разбиение $[a,b]$: $a = x_0 < x_1 < \ldots < x_n = b$, такое что $f$ интегрируема по Риману на $[ \alpha, \beta] \subset (x_i, x_{i+1})$. Если это так, то можем определить интеграл:
	\[ \int\limits_{x_i}^{x_{i+1}} f(t) \, dt \eqDef \lim_{\substack{\alpha \ra x_i \\ \beta \ra x_{i+1}}} \int\limits_\alpha^\beta f(t) \, dt. \]
Во-вторых, пусть пределы существуют и конечны для всех $i$. \par
Тогда (при этих двух условиях) определяем интеграл на $[a,b]$:
	\[ \int\limits_a^b f(t) \, dt \eqDef \sum_{i=1}^n \int\limits_{x_i}^{x_{i+1}} f(t) \, dt. \]
Аналогично про абсолютную сходимость. Условия: интегрируемость для $f$, и существование конечных пределов для $|f|$. \par
Нас сильно уведёт в сторону проверка этого, поэтому -- не будем. 

\begin{definition}
$f \colon [a,b] \ra \R$ абсолютно интегрируема, если $\int_a^b |f(t)| \, dt$ сходится.
\end{definition}

\begin{definition}
$f \colon [-\pi, \pi] \ra \R$ абсолютно интегрируема.
	\[ a_n = \frac{1}{\pi} \int\limits_{-\pi}^\pi f(x) \cos (nx) \, dx, \qquad b_n = \frac{1}{\pi} \int\limits_{-\pi}^\pi f(x) \sin (nx) \, dx. \]
Тогда 
	\[ \frac{a_0}{2} + \sum_{n=1}^{+\infty} ( a_n \cos nx + b_n \sin nx ) \quad - \quad \text{ряд Фурье для функции $f$.} \]
$a_n$ и $b_n$ -- коэффициенты Фурье.
\end{definition}

\begin{theorem}
Равномерно сходящийся ряд -- ряд Фурье своей суммы.
\end{theorem}

\begin{proof}
Смотри предыдущую теорему. 
\end{proof}

\begin{warning}
$f \colon [-\pi, \pi] \ra \R$. \par
Выкинем значение в точке $\pi$, а $f \bigl|_{[-\pi,\pi)}$ продолжим до $2\pi$-периодической функции. \par
При этом, если $f \in C[-\pi, \pi]$, то непрерывность, очевидно, может нарушиться. То есть $2\pi$-периодическое продолжение непрерывно во всех точках, кроме точек $\pi + 2 \pi k$ ($k \in \Z$). Если $f(-\pi) = f(\pi)$, то $2\pi$-периодическое продолжение будет непрерывным. \par
Продолжение (абсолютной) интегрируемости не портит (если исходная функция была (абсолютно) интегрируема). Дальше всё время будем думать об $f$ как о её $2\pi$-периодическом продолжении. \par
Если $f$ $2\pi$-периодична, то
	\[ a_n = \frac{1}{\pi} \int\limits_y^{y+2\pi} f(x) \cos (nx) \, dx, \qquad b_n = \frac{1}{\pi} \int\limits_y^{y+2\pi} f(x) \sin (nx) \, dx. \]
\end{warning}

\begin{definition}
$f \colon E \ra \R$. 
\begin{enumerate}
\item $ \supp f = \cl \, \{ \, x \in E : f(x) \not= 0 \, \} $ -- носитель функции.
\item $f$ -- финитная, если $\supp f$ -- ограниченное множество (т.е. $\supp f$ -- компакт). 
\end{enumerate}
\end{definition}

\begin{example}
Характеристическая функция ограниченного множества:
	\[ \1_E (x) =  
	\begin{cases}
		1, \: x \in E \\
		0, \: x \notin E.
	\end{cases} \]		
\end{example}

\begin{example}
Ступенчатые функции -- (конечные) линейные комбинации характеристических функций отрезков:
	\[ f(x) = \sum_{i=1}^n \lambda_i \1_{[a_i,b_i)} \quad \text{(удобнее писать не отрезки, а полуинтервалы).} \]
\end{example}

\begin{warning}
Полуинтервалы можно считать попарно непересекающимися (пересекающиеся нарежем и просуммируем коэффициенты на пересечениях).
\end{warning}

\begin{warning}
	\[ \int\limits_{-\infty}^{+\infty} f(x) \, dx = \sum_{i=1}^n \lambda_i (b_i - a_i). \]
\end{warning}

\begin{lemma}
$f \colon \langle a,b \rangle \ra \R$ абсолютно интегрируема ($a,b \in \overline{\R}$). \par
Тогда существует последовательность финитных ступенчатых функций $\phi_n$ такая, что $\supp \phi_n \subset (a,b)$, и 
	\[ \lim_{n \ra \infty} \int\limits_a^b |f(x) - \phi_n(x)| \, dx = 0. \]
\end{lemma}

\begin{proof}
Можно считать, что особые точки только в $a$ и $b$, т.е. $f$ абсолютно интегрируема на $[\alpha, \beta] \in (a,b)$ (иначе берём разбиение из абсолютной интегрируемости -- см. отступление про сходимость интегралов):
	\[ \int\limits_a^b | f(x) | \, dx = \lim_{\substack{\alpha \ra a \\ \beta \ra b}} \int\limits_\alpha^\beta | f(x) | \, dx. \]
Подберём $\alpha$ и $\beta$ так, чтобы
	\[ \int\limits_a^\alpha | f(x) | \, dx + \int\limits_\beta^b | f(x) | \, dx < \eps. \]
На $[\alpha, \beta]$ $f$ интегрируема по Риману. Значит,
\begin{gather*}
	\exists \delta > 0, \forall \tau \text{ -- разб.}, |\tau| < \delta \text{ и } \forall \text{оснащ. } \xi \colon \left| \int\limits_\alpha^\beta f(x)\, dx - \sum_{k=1}^n f( \xi_k ) (x_k - x_{k-1}) \right| < \eps. \\
	 f( \xi_k ) (x_k - x_{k-1}) = \int\limits_\alpha^\beta f( \xi_k ) \1_{[x_{k-1},x_k)} (x) \, dx \quad \Ra \quad \left| \int\limits_\alpha^\beta \left( f(x) - \sum_{k=1}^n f( \xi_k ) \1_{[x_{k-1},x_k)} (x) \right) dx \right| < \eps, 
\end{gather*}
для $\forall \xi_k \in [x_{k-1}, x_k]$. Стало быть,
	\[ \left| \int\limits_\alpha^\beta \underbrace{\left( f(x) - \sum_{k=1}^n \underset{[x_{k-1},x_k]}{\inf f} \1_{[x_{k-1},x_k)} (x) \right)}_{\geq 0} dx \right| \leq \eps. \]
Подынтегральная функция неотрицательна для $\forall x \in [\alpha, \beta]$, т.к. если $x \in [x_{k-1},x_k)$ (для некоторого $k$), то сравниваем $f(x)$ и $\underset{[x_{k-1},x_k]}{\inf f}$. \par
Пусть 
	\[ \phi(x) = \sum_{k=1}^n \underset{[x_{k-1},x_k]}{\inf f} \1_{[x_{k-1},x_k)} (x). \]
Тогда
	\[ \int\limits_\alpha^\beta | f(x) - \phi(x) | \, dx \leq \eps. \]
Вне $[\alpha, \beta]$ функция $\phi = 0$, следовательно
	\[ \int\limits_a^\alpha | f(x) - \phi(x) | \, dx + \int\limits_\beta^b | f(x)- \phi(x) | \, dx = \int\limits_a^\alpha | f(x) | \, dx + \int\limits_\beta^b | f(x) | \, dx < \eps. \]
Исходя из двух предыдущих неравенств,
	\[ \int\limits_a^b |f(x) - \phi(x)| \, dx < 2\eps. \]
Искомые $\phi_n$ -- это $\phi$, построенные по $\eps = \frac{1}{2n}$. 
\end{proof}

\begin{warning}
Если $|f(x)| \leq C$ для $\forall x \in \langle a,b \rangle$, то $|\phi_n(x)| \leq C$ для $\forall x \in \R$.
\end{warning}

\begin{theorem}
(Лемма Римана-Лебега.) \\
$f \colon (a,b) \ra \R$ абсолютно интегрируема ($a,b \in \overline{\R}$). \par
Тогда
	\[ \lim_{p \ra +\infty} \int\limits_a^b f(x) \cos px \, dx = \lim_{p \ra +\infty} \int\limits_a^b f(x) \sin px \, dx = \lim_{p \ra +\infty} \int\limits_a^b f(x) e^{ipx} \, dx = 0. \]
\end{theorem}

\begin{consequence}
Коэффициенты Фурье абсолютно интегрируемой функции стремятся к нулю.
\end{consequence}

\begin{proof}
\textbf{Шаг 1.} Докажем теорему, когда $f(x) = \1_{[\alpha,\beta]}$, где $[\alpha,\beta] \subset (a,b)$:
	\[ \int\limits_a^b f(x) \cos px \, dx = \int\limits_a^b \1_{[\alpha,\beta]} \cos px \, dx = \int\limits_\alpha^\beta \cos px \, dx = \frac{1}{p} (\sin \beta p - \sin \alpha p). \]
Следовательно
	\[ \left| \int\limits_a^b f(x) \cos px \, dx \right| = \frac{|\sin \beta p - \sin \alpha p|}{p} \leq \frac{2}{p} \ra 0. \]
Для интеграла с синусом доказывается аналогично.  
\end{proof}