\setauthor{ Белоусов Н. }

\begin{theorem}
(Признак Вейерштрасса.) \\
$f, g \colon [a, +\infty) \times T \ra \R$ непрерывны на $[a, +\infty)$ для $\forall t \in T$. \\
Если $| f(x,t) | \leq g(x,t)$ при $\forall x,t$; $\int\limits_a^{+\infty} g(x,t) \, dx$ равномерно сходится, то и $\int\limits_a^{+\infty} f(x,t) \, dx$  равномерно сходится.
\end{theorem}

\begin{proof}
Равномерная сходимость $\int_a^{+\infty} g(x,t) \, dx$ по критерию Коши:
\begin{equation*}
	\forall \eps > 0 \, \exists b, \forall B, C > b, \forall t \in T \colon \int\limits\limits_B^C g(x,t) \, dx < \eps.
\end{equation*}
При этом
\begin{equation*}
	\left| \int\limits\limits_B^C f(x,t) \, dx \right| \leq \int\limits\limits_B^C | f(x,t) | \, dx \leq \int\limits\limits_B^C g(x,t) \, dx < \eps,
\end{equation*}
то есть получили критерий Коши для $\int_a^{+\infty} f(x,t) \, dx$.
\end{proof}

\begin{consequence}
$f \colon [a, +\infty) \times T \ra \R$ непрерывна на $[a, +\infty)$ для $\forall t \in T$. \\
$g \colon [a, +\infty) \ra \R$ непрерывна и $\int_a^{+\infty} g(x) \, dx$ сходится. Если $| f(x,t) | \leq g(x)$ при $\forall x,t$, то $\int_a^{+\infty} f(x,t) \, dx$ равномерно сходится.
\end{consequence}

\begin{example}
\[ \int\limits\limits_0^{+\infty} \frac{\cos tx}{1+x^2} \, dx \text{ -- сходится равномерно на $\R$:} \]
\[ \left| \frac{\cos tx}{1+x^2} \right| \leq \frac{1}{1+x^2}, \qquad \int\limits\limits_0^{+\infty} \frac{1}{1+x^2} \, dx \text{ -- сходится.} \]
\end{example}

\begin{theorem}
(Признак Дирихле.) \\
$f, g \colon [a, +\infty) \times T \ra \R$ непрерывны на $[a, +\infty)$ для $\forall t \in T$.
\begin{enumerate}
\item $\exists M, \forall t, \forall b \colon \left| \int\limits_a^b f(x,t) \, dx \right| \leq M$.
\item $\forall t \in T$ функция $g(x,t)$ монотонна; $g(x,t) \rra 0$ при $x \ra +\infty$ (равномерно по $t \in T$).
\end{enumerate}
Тогда $\int\limits_a^{+\infty} f(x,t) g(x,t) \, dx$ равномерно сходится.
\end{theorem}

\begin{warning}
Мы докажем для случая, когда $g$ непрерывно дифференцируема. Теорема справедлива и без дифференцируемости, но доказательство сложнее.
\end{warning}

\begin{proof}
Рассмотрим \[ F(y, t) = \int\limits_a^y f(x,t) \, dx \quad \Ra \quad \forall y, t \colon | F(y,t) | \leq M. \]
По частям:
\begin{equation*}
\int\limits_a^b f(x,t) g(x,t) \, dx = F(x,t) g(x,t) \biggl|_{x=a}^{x=b} - \int\limits_a^b F(x,t) g'_x (x,t) \, dx.
\end{equation*}
$\underbrace{F(b,t)}_{\text{равн. огр.}} \underbrace{g(b,t)}_{\rra 0} \rra 0$ при $b \ra +\infty$ (равномерно по $t \in T$). \\
Осталось понять, что $\int\limits_a^{+\infty} F(x,t) g'_x(x,t) \, dx$ сходится равномерно. Это следует из того, что
\begin{gather*}
| F(x,t) g'_x(x,t) | \leq M | g'_x(x,t) |, \\
\int\limits_a^b | g'_x (x,t) | \, dx = (g \text{ -- мон.}) = \left| \int\limits_a^b g'_x (x,t) \, dx \right| = | g(b,t) - g(a,t) | \rra | g(a,t) |.
\end{gather*}
\end{proof}

\begin{theorem}
(Признак Абеля.) \\
$f, g \colon [a, +\infty) \times T \ra \R$ непрерывны на $[a, +\infty)$ для $\forall t \in T$.
\begin{enumerate}
\item $\int\limits_a^{+\infty} f(x,t) \, dx$ равномерно сходится.
\item $\forall t \in T \colon g(x,t)$ монотонна; $\exists M, \forall x, t \colon | g(x,t) | \leq M$.
\end{enumerate}
Тогда $\int\limits_a^{+\infty} f(x,t) g(x,t) \, dx$ равномерно сходится.
\end{theorem}

\begin{warning}
Доказательство опять-таки для случая непрерывно дифференцируемой $g$.
\end{warning}

\begin{proof}
Поймём, что $\int\limits_B^{+\infty} f(x,t) g(x,t) \, dx \rra 0$ при $B \ra +\infty$. Пусть $F$ -- та же, что в признаке Дирихле. Тогда
\[	\int\limits_B^{+\infty} f(x,t) g(x,t) \, dx = F(x,t) g(x,t) \biggl|_{x=B}^{x=+\infty} - \int\limits_B^{+\infty} F(x,t) g'_x(x,t) \, dx = ... \]
Преобразуем первое слагаемое:
\begin{align*}	
	F(x,t) g(x,t) \biggl|_{x=B}^{x=+\infty} & = F(+\infty, t) g(+\infty, t) - F(B,t) g(B,t) =  \\
	& = F(+\infty, t) [ g(+\infty,t) - g(B,t) ] + \underbrace{g(B,t)}_{\text{равн. огр.}} \underbrace{[ F(+\infty,t) - F(B,t) ]}_{\rra 0}, 
\end{align*}
следовательно
\[ ... = \underbrace{g(B,t) [ F(+\infty,t) - F(B,t) ]}_{\rra 0} + \int\limits_B^{+\infty} \bigl(F(+\infty,t) - F(x,t)\bigl) g'_x(x,t) \, dx \]
Почему второе слагаемое $\rra 0$:
\[ F(+\infty,t) - F(x,t) = \int\limits_x^{+\infty} f(y,t) \, dy \rra 0 \text{ при } x \ra +\infty \quad \Ra \quad \exists b, \forall x>b, \forall t \colon \left| \int\limits_x^{+\infty} f(y,t) \, dy \right| < \eps, \]
стало быть
\[ \left| \int\limits_B^{+\infty} \bigl( F(+\infty,t) - F(x,t) \bigl) g'_x(x,t) \, dx \right| \leq \int\limits_B^{+\infty} \left| \int\limits_x^{+\infty} f(y,t) \, dy \right| \cdot | g'_x (x,t) | \, dx \leq \eps \int\limits_B^{+\infty} | g'_x(x,t) | \, dx = ... \]
Вспоминаем, что $g$ монотонна:
\[ ... = \eps \left| \int\limits_B^{+\infty} g'_x(x,t) \, dx \right| = \eps | g(+\infty,t) - g(B,t) | \leq 2M \eps. \]
\end{proof}

\begin{example}
\[ \int\limits_1^{+\infty} \dfrac{\sin x}{x^t} \, \]
\begin{enumerate}
\item $t \geq t_0 > 0$ -- равномерно сходится.

\begin{proof}
Пусть $f(x,t) = \sin x$, тогда $\int\limits_1^{+\infty} f(x,t) \, dx$ -- равномерно ограничен. \\
$g(x,t) = \frac{1}{x^t}$ -- монотонна при любом фиксированном $t$; равномерно сходится к нулю:
\[ \left| \dfrac{1}{x^t} \right| \leq \dfrac{1}{x^{t_0}} \ra 0 \quad \Ra \quad \dfrac{1}{x^t} \rra 0 \text{ при } x \ra +\infty. \]
Значит, исходный интеграл равномерно сходится по признаку Дирихле.
\end{proof}

\item $t > 0$ -- нет равномерной сходимости.

\begin{proof}
Расходится при $t=0$, отсюда -- по следствию 1.2.1 -- нет равномерной сходимости.
\end{proof}

\end{enumerate}
\end{example}

\begin{theorem}
$f \colon [a, +\infty) \times T \ra \R$ непрерывна на $[a, +\infty), \forall t \in T$. \\
Для любого $[a, b]$: $f(x,t) \rra \phi(x)$ при $t \ra t_0$ (равномерно по $x \in [a, b]$), и $\int\limits_a^{+\infty} f(x,t) \, dx$ сходится равномерно по $t \in T$. Тогда
\[ \lim_{t \ra t_0} \int\limits_a^{+\infty} f(x,t) \, dx = \int\limits _a^{+\infty} \phi(x) \, dx, \]
и последний интеграл сходится.
\end{theorem}

\begin{proof}
Докажем, что $\int\limits_a^{+\infty} \phi(x) \, dx$ сходится по признаку Коши. \\
$\int\limits_a^{+\infty} f(x,t) \, dx$ равномерно сходится:
\[\forall \eps > 0 \, \exists b > a, \forall B, C > b, \forall t \in T \colon \left| \int\limits_B^C f(x,t) \, dx \right| < \eps. \]
\[ \left| \int\limits_B^C \phi(x) \, dx \right| \leq \left| \int\limits_B^C \underbrace{\bigl(\phi(x) - f(x,t) \bigl)}_{\rra 0 \text{ на } [a,C]} \, dx \right| + \left| \int\limits_B^C f(x,t) \, dx \right| < 2\eps, \]
то есть $\int\limits_a^{+\infty} \phi$ сходится.
Теперь про предел:
\begin{gather*}
	\left| \int\limits_a^{+\infty} f(x,t) \, dx - \int\limits_a^{+\infty} \phi(x) \, dx \right| \leq \left| \int\limits_a^b \bigl( f(x,t) - \phi(x) \bigl) \, dx \right| + \left| \int\limits_b^{+\infty} \phi(x) \, dx \right| + \left| \int\limits_b^{+\infty} f(x,t) \, dx \right|. \\
	\int\limits_a^{+\infty} \phi(x) \, dx \text{ -- сходится} \quad \Ra \quad \exists b_0, \forall b>b_0 \colon \left| \int\limits_b^{+\infty} \phi(x) \, dx \right| < \eps; \\
	\int\limits_a^{+\infty} f(x,t) \, dx \text{ -- равномерно сходится} \quad \Ra \quad \exists b_1, \forall b>b_1, \forall t \in T \colon \left| \int\limits_b^{+\infty} f(x,t) \, dx \right| < \eps.
\end{gather*}
Выбираем $b$ такое, что $b > b_0$, $b > b_1$. Ещё знаем, что $f(x,t) \rra \phi(x)$ при $t \ra t_0$ (равномерно по $x \in [a,b]$). Тогда выберем столь малую окрестность $t_0$, что при $t$ оттуда
\begin{equation*}
	| f(x,t) - \phi(x)| < \frac{\eps}{b-a}, \, \forall x \in [a,b] \quad \Ra \quad \left| \int\limits_a^b (f(x,t) - \phi(x)) \, dx \right| \leq \int\limits_a^b | f(x,t) - \phi(x) | \, dx < \eps.
\end{equation*}
\end{proof}

\begin{warning}
Без равномерности неверно.
\end{warning}

\begin{example}
\-
\begin{multicols}{2}	
\begin{tikzpicture}[scale=1.3]
    %Сетка
    \draw[very thin,color=gray] (-.9,-.9) grid (3.5,1.9);
    %Оси
    \draw[->](-.5,0) -- (4, 0) node[below]{$x$};
    \draw[->](0,-.5) -- (0, 2) node[left]{$y$};
    %График
    \draw[blue, very thick](0,1) -- (1, 1) -- (2, 0) -- (3, 0);
    %Подписи на осях
    \draw(0,1) node[left]{$\dfrac{1}{t}$};
    \draw(1,0) node[below]{$t$};
    \draw(2,0) node[below]{$t+1$};
\end{tikzpicture}
\columnbreak
\[ f(x,t) =
	\begin{cases}
		\frac{1}{t} \text{ при } x \in [0,t] \\
		\text{лин. при } x \in [t, t+1] \\
		0 \text{, иначе}
	\end{cases} \]
Понятно, что $f(x,t) \rra 0$ при $t \ra +\infty$. Но
\[ \int\limits_0^{+\infty} f(x,t) \, dx = 1 + \frac{1}{2t} \underset{t \ra +\infty}{\longrightarrow} 1 \not= \int\limits_0^{+\infty} \lim_{t \ra +\infty} f(x,t) \, dx. \]
\end{multicols}

\end{example}

\begin{theorem}
$ f \colon [a, +\infty) \times [c, d] \ra \R$ непрерывна. \\
$F(t) = \int\limits_a^{+\infty} f(x,t) \, dx$ равномерно сходится по $t \in [c, d]$. Тогда $F \in C[c, d]$.
\end{theorem}

\begin{proof}
\[ F_b(t) = \underbrace{\int\limits_a^b f(x,t) \, dx}_{\text{непр. из т. 1.1.1}} \rra F(t) \]
равномерно по $t \in [c, d]$, значит, $F(t)$ непрерывна.
\end{proof}

\begin{theorem}
$f \colon [a, +\infty) \times [c, d] \ra \R$ непрерывна; $f'_t \in C \bigl( [a, +\infty) \times [c, d] \bigl)$. \\
$\Phi(t) = \int\limits_a^{+\infty} f'_t (x,t) \, dx$ равномерно сходится на $t \in [c, d]$. \\
$F(t) = \int\limits_a^{+\infty} f(x,t) \, dx$ сходится при некотором $t = t_0$. \\
Тогда $\int\limits_a^{+\infty} f(x,t)$ сходится равномерно, и $F'(t) = \Phi(t)$.
\end{theorem}

\begin{proof}
Рассмотрим
\[ F_b(t) = \int\limits_a^b f(x,t) \, dx, \quad \Phi_b(t) = \int\limits_a^b f'_t(x,t) \, dx. \]
$F_b(t)$ -- дифференцируемая функция, и $F'_b(t) = \Phi_b(t) \rra \Phi(t)$ равномерно по $t \in [c,d]$ (см. теорему 1.1.2). Получаем, что
\[ F_b(t) = \int\limits_{t_0}^t F'_b(s) \, ds + F_b(t_0). \]
Пусть
\[ G(t) = \int\limits_{t_0}^t \Phi(s) \, ds + F(t_0), \]
тогда:
\[ | F_b(t) - G(t) | \leq \int\limits_{t_0}^t \underbrace{| F'_b(s) - \Phi(s) |}_{\rra 0} \, ds + \underbrace{| F_b(t_0) - F(t_0) |}_{\ra 0}. \]
То есть $F_b(t) \rra G(t)$, и $G(t) = F(t)$, а значит,
\[ F'(t) = G'(t) = \left( \int\limits_{t_0}^t \Phi(s) \, ds + F(t_0) \right)'_t = \Phi(t). \]
\end{proof}

